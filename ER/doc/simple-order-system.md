[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![ER](../data/img/entity_relation.png)

</details>

---

<h1>Simple Order System</h1>

<details>
  <summary>Show Physical ERM</summary>

![simple-order-system](../data/img/simple-order-system.jpg)

</details>

---

<h1>Physical ERM to SQL</h1>

<details>
  <summary>Show SQL Script</summary>

```sql

CREATE TABLE IF NOT EXISTS Customer (
  ID                     SERIAL NOT NULL, 
  EmployeeID             int4 NOT NULL, 
  Sales_Rep_Employee_Num int4, 
  Last_Name              varchar(255), 
  First_Name             varchar(255), 
  Phone                  varchar(255), 
  Address1               varchar(255), 
  Address2               varchar(255), 
  City                   varchar(255), 
  State                  varchar(255), 
  Postal_Code            int4, 
  Country                varchar(255), 
  Credit_Limit           numeric(19, 0), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Employee (
  ID         SERIAL NOT NULL, 
  OfficeCode int4 NOT NULL, 
  Reports_To int4 NOT NULL, 
  Last_Name  varchar(255), 
  First_Name varchar(255), 
  Extension  varchar(255), 
  Email      varchar(255), 
  Job_Title  varchar(100), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Office (
  Code        SERIAL NOT NULL, 
  City        varchar(255), 
  Phone       varchar(255), 
  Address1    varchar(255), 
  Address2    varchar(255), 
  State       varchar(255), 
  Country     varchar(255), 
  Postal_Code varchar(10), 
  Territory   varchar(200), 
  PRIMARY KEY (Code));

CREATE TABLE IF NOT EXISTS "Order" (
  ID            numeric(10, 0) NOT NULL, 
  CustomerID    int4 NOT NULL, 
  Order_Date    date, 
  Requered_Date date, 
  Shipped_Date  date, 
  Status        int4, 
  Comments      varchar(255), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Order_Product (
  ID          int4 NOT NULL, 
  OrderID     numeric(10, 0) NOT NULL, 
  ProductCode int4 NOT NULL, 
  Qyt         int4, 
  Prece_Each  numeric(19, 0), 
  PRIMARY KEY (ID, 
  OrderID, 
  ProductCode));

CREATE TABLE IF NOT EXISTS Payment (
  Check_Num    varchar(255) NOT NULL, 
  CustomerID   int4 NOT NULL, 
  Payment_Date date, 
  Amount       numeric(19, 0), 
  PRIMARY KEY (Check_Num));

CREATE TABLE IF NOT EXISTS Product (
  Code            SERIAL NOT NULL, 
  ProductlineID   int4 NOT NULL, 
  Name            varchar(255), 
  "Scale  int"    int4, 
  Vendor          varchar(255), 
  Pdt_Description varchar(255), 
  Qty_In_Stock    int4, 
  Buy_Price       numeric(19, 0), 
  MSRP            varchar(255), 
  PRIMARY KEY (Code));

CREATE TABLE IF NOT EXISTS Productline (
  ID          SERIAL NOT NULL, 
  Des_In_Text varchar(255), 
  Des_In_HTML varchar(100), 
  PRIMARY KEY (ID));

ALTER TABLE Product ADD CONSTRAINT FKProduct_1 FOREIGN KEY (ProductlineID) REFERENCES Productline (ID);

ALTER TABLE Employee ADD CONSTRAINT FKEmployee_1 FOREIGN KEY (Reports_To) REFERENCES Employee (ID);

ALTER TABLE Employee ADD CONSTRAINT FKEmployee_2 FOREIGN KEY (OfficeCode) REFERENCES Office (Code);

ALTER TABLE Customer ADD CONSTRAINT FKCustomer_1 FOREIGN KEY (EmployeeID) REFERENCES Employee (ID);

ALTER TABLE Order_Product ADD CONSTRAINT FKOrder_Prod_1 FOREIGN KEY (ProductCode) REFERENCES Product (Code);

ALTER TABLE "Order" ADD CONSTRAINT FKOrder_1 FOREIGN KEY (CustomerID) REFERENCES Customer (ID);

ALTER TABLE Order_Product ADD CONSTRAINT FKOrder_Prod_2 FOREIGN KEY (OrderID) REFERENCES "Order" (ID);

ALTER TABLE Payment ADD CONSTRAINT FKPayment_1 FOREIGN KEY (CustomerID) REFERENCES Customer (ID);

```
</details>