[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![ER](../data/img/entity_relation.png)

</details>

---

<h1>Favorited Team Statistics</h1>

<details>
  <summary>Show Physical ERM</summary>

![favorited-team-statistics](../data/img/favorited-team-statistics.jpg)

</details>

---

<h1>Physical ERM to SQL</h1>

<details>
  <summary>Show SQL Script</summary>

```sql

CREATE TABLE IF NOT EXISTS Match (
  ID         SERIAL NOT NULL, 
  Match_Date date, 
  Stadium    int4, 
  Opponent   int4, 
  Own_Score  int4, 
  Opp_Score  int4, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Match_Player (
  MatchID  int4 NOT NULL, 
  PlayerID int4 NOT NULL, 
  Score    varchar(10), 
  PRIMARY KEY (MatchID, 
  PlayerID));

CREATE TABLE IF NOT EXISTS Player (
  ID           SERIAL NOT NULL, 
  Name         varchar(255), 
  Age          int4, 
  Season_Score int4, 
  PRIMARY KEY (ID));

ALTER TABLE Match_Player ADD CONSTRAINT FKMatch_Play_1 FOREIGN KEY (PlayerID) REFERENCES Player (ID);

ALTER TABLE Match_Player ADD CONSTRAINT FKMatch_Play_2 FOREIGN KEY (MatchID) REFERENCES Match (ID);

```
</details>