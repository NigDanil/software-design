[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![ER](../data/img/entity_relation.png)

</details>

---

<h1>Production Tracking System</h1>

<details>
  <summary>Show Physical ERM</summary>

![production-tracking-system](../data/img/production-tracking-system.jpg)

</details>

---

<h1>Physical ERM to SQL</h1>

<details>
  <summary>Show SQL Script</summary>

```sql

CREATE TABLE IF NOT EXISTS Lot (
  Number      SERIAL NOT NULL, 
  Create_Date date, 
  Cost        numeric(19, 0), 
  PRIMARY KEY (Number));

CREATE TABLE IF NOT EXISTS Product_Units (
  SN           SERIAL NOT NULL, 
  LotNumber    int4 NOT NULL, 
  Weight       int4, 
  Product_Type int4, 
  Product_Desc varchar(255), 
  Quality_Test int4, 
  PRIMARY KEY (SN));

CREATE TABLE IF NOT EXISTS Raw_Material (
  ID       SERIAL NOT NULL, 
  Type     char(1), 
  Uni_Cost numeric(19, 0), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Raw_Material_Lot (
  Units          int4, 
  Raw_MaterialID int4 NOT NULL, 
  LotNumber      int4 NOT NULL, 
  PRIMARY KEY (Raw_MaterialID, 
  LotNumber));

ALTER TABLE Raw_Material_Lot ADD CONSTRAINT FKRaw_Materi_1 FOREIGN KEY (Raw_MaterialID) REFERENCES Raw_Material (ID);

ALTER TABLE Raw_Material_Lot ADD CONSTRAINT FKRaw_Materi_2 FOREIGN KEY (LotNumber) REFERENCES Lot (Number);

ALTER TABLE Product_Units ADD CONSTRAINT FKProduct_Un_1 FOREIGN KEY (LotNumber) REFERENCES Lot (Number);

```
</details>