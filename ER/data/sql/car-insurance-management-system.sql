CREATE TABLE IF NOT EXISTS Accident (
  ReportNum    SERIAL NOT NULL, 
  Location     varchar(255), 
  AccidentDate date, 
  PRIMARY KEY (ReportNum));

CREATE TABLE IF NOT EXISTS Car (
  License        SERIAL NOT NULL, 
  PersonDriverID int4 NOT NULL, 
  Model          varchar(255), 
  Year           int4, 
  PRIMARY KEY (License));

CREATE TABLE IF NOT EXISTS Car_Accident (
  CarLicense        int4 NOT NULL, 
  AccidentReportNum int4 NOT NULL, 
  DamageAmount      numeric(19, 0), 
  PRIMARY KEY (CarLicense, 
  AccidentReportNum));

CREATE TABLE IF NOT EXISTS Person (
  DriverID SERIAL NOT NULL, 
  Address  varchar(255), 
  Name     varchar(255), 
  PRIMARY KEY (DriverID));

ALTER TABLE Car ADD CONSTRAINT FKCar_1 FOREIGN KEY (PersonDriverID) REFERENCES Person (DriverID);

ALTER TABLE Car_Accident ADD CONSTRAINT FKCar_Accide_1 FOREIGN KEY (CarLicense) REFERENCES Car (License);

ALTER TABLE Car_Accident ADD CONSTRAINT FKCar_Accide_2 FOREIGN KEY (AccidentReportNum) REFERENCES Accident (ReportNum);