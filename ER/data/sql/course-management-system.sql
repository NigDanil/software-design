CREATE TABLE IF NOT EXISTS Course (
  ID              SERIAL NOT NULL, 
  DepartamentName varchar(255) NOT NULL, 
  InstructorID    int4 NOT NULL, 
  Duration        int4, 
  Name            varchar(255), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Course_Student (
  StudentID int4 NOT NULL, 
  CourseID  int4 NOT NULL, 
  PRIMARY KEY (StudentID, 
  CourseID));

CREATE TABLE IF NOT EXISTS Departament (
  Name     varchar(255) NOT NULL, 
  Location varchar(255), 
  PRIMARY KEY (Name));

CREATE TABLE IF NOT EXISTS Instructor (
  ID              SERIAL NOT NULL, 
  DepartamentName varchar(255) NOT NULL, 
  First_Name      varchar(255), 
  Last_Name       varchar(255), 
  Phone           varchar(255), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Student (
  ID         SERIAL NOT NULL, 
  First_Name varchar(255), 
  Last_Name  varchar(255), 
  Phone      varchar(255), 
  PRIMARY KEY (ID));

ALTER TABLE Instructor ADD CONSTRAINT FKInstructor_1 FOREIGN KEY (DepartamentName) REFERENCES Departament (Name);

ALTER TABLE Course ADD CONSTRAINT FKCourse_1 FOREIGN KEY (DepartamentName) REFERENCES Departament (Name);

ALTER TABLE Course ADD CONSTRAINT FKCourse_2 FOREIGN KEY (InstructorID) REFERENCES Instructor (ID);

ALTER TABLE Course_Student ADD CONSTRAINT FKCourse_Stu_1 FOREIGN KEY (StudentID) REFERENCES Student (ID);

ALTER TABLE Course_Student ADD CONSTRAINT FKCourse_Stu_2 FOREIGN KEY (CourseID) REFERENCES Course (ID);