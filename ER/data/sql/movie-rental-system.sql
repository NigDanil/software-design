CREATE TABLE IF NOT EXISTS Actor (
  ID          SERIAL NOT NULL, 
  First_Name  varchar(255), 
  Last_Name   varchar(255), 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Address (
  ID          SERIAL NOT NULL, 
  CityID      int4 NOT NULL, 
  Address     varchar(50), 
  Address2    varchar(50), 
  District    int4, 
  Postal_Code varchar(10), 
  Phone       varchar(20), 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Categoty (
  ID          SERIAL NOT NULL, 
  Name        varchar(25), 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS City (
  ID          SERIAL NOT NULL, 
  CountryID   int4 NOT NULL, 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Country (
  ID          SERIAL NOT NULL, 
  Coutry      varchar(50), 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Customer (
  ID          SERIAL NOT NULL, 
  AddressID   int4 NOT NULL, 
  First_Name  varchar(255), 
  Last_Name   varchar(255), 
  Email       varchar(50), 
  Active      char(1), 
  Create_Date date, 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Film (
  ID               SERIAL NOT NULL, 
  LanguageID       int4 NOT NULL, 
  Title            varchar(255), 
  Description      varchar(255), 
  Release_Year     int4, 
  Rental_Duration  int4, 
  Rental_Rate      numeric(19, 0), 
  Length           int4, 
  Replacement_Cost numeric(19, 0), 
  Rating           int4, 
  Last_Update      date, 
  Special_Features varchar(255), 
  "Fulltext"       varchar(255), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Film_Actor (
  ActorID     int4 NOT NULL, 
  FilmID      int4 NOT NULL, 
  Last_Update date);

CREATE TABLE IF NOT EXISTS Film_Category (
  Last_Update date, 
  CategotyID  int4 NOT NULL, 
  FilmID      int4);

CREATE TABLE IF NOT EXISTS Inventory (
  ID          SERIAL NOT NULL, 
  FilmID      int4 NOT NULL, 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Language (
  ID          SERIAL NOT NULL, 
  Name        varchar(20), 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Payment (
  ID           SERIAL NOT NULL, 
  RentalID     int4 NOT NULL, 
  CustomerID   int4 NOT NULL, 
  Amount       numeric(19, 0), 
  Payment_Date date, 
  StaffID      int4 NOT NULL, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Rental (
  ID          SERIAL NOT NULL, 
  StaffID     int4 NOT NULL, 
  CustomerID  int4 NOT NULL, 
  InventoryID int4 NOT NULL, 
  Rental_Date date, 
  Return_Date date, 
  Last_Update date, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Staff (
  ID          SERIAL NOT NULL, 
  AddressID   int4 NOT NULL, 
  StoreID     int4 NOT NULL, 
  PaymentID   int4, 
  First_Name  varchar(255), 
  Last_Name   varchar(255), 
  Email       varchar(50), 
  Active      char(1), 
  Password    varchar(40), 
  Last_Update date, 
  PictureURL  varchar(80), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Store (
  ID          SERIAL NOT NULL, 
  Last_Update date, 
  AddressID   int4 NOT NULL, 
  PRIMARY KEY (ID));

ALTER TABLE Film_Category ADD CONSTRAINT FKFilm_Categ_1 FOREIGN KEY (CategotyID) REFERENCES Categoty (ID);

ALTER TABLE Film_Category ADD CONSTRAINT FKFilm_Categ_2 FOREIGN KEY (FilmID) REFERENCES Film (ID);

ALTER TABLE Film ADD CONSTRAINT FKFilm_1 FOREIGN KEY (LanguageID) REFERENCES Language (ID);

ALTER TABLE Film_Actor ADD CONSTRAINT FKFilm_Actor_1 FOREIGN KEY (ActorID) REFERENCES Actor (ID);

ALTER TABLE Film_Actor ADD CONSTRAINT FKFilm_Actor_2 FOREIGN KEY (FilmID) REFERENCES Film (ID);

ALTER TABLE Inventory ADD CONSTRAINT FKInventory_1 FOREIGN KEY (FilmID) REFERENCES Film (ID);

ALTER TABLE Rental ADD CONSTRAINT FKRental_1 FOREIGN KEY (InventoryID) REFERENCES Inventory (ID);

ALTER TABLE Payment ADD CONSTRAINT FKPayment_1 FOREIGN KEY (RentalID) REFERENCES Rental (ID);

ALTER TABLE Rental ADD CONSTRAINT FKRental_2 FOREIGN KEY (StaffID) REFERENCES Staff (ID);

ALTER TABLE Staff ADD CONSTRAINT FKStaff_1 FOREIGN KEY (StoreID) REFERENCES Store (ID);

ALTER TABLE Rental ADD CONSTRAINT FKRental_3 FOREIGN KEY (CustomerID) REFERENCES Customer (ID);

ALTER TABLE Payment ADD CONSTRAINT FKPayment_2 FOREIGN KEY (CustomerID) REFERENCES Customer (ID);

ALTER TABLE Customer ADD CONSTRAINT FKCustomer_1 FOREIGN KEY (AddressID) REFERENCES Address (ID);

ALTER TABLE Staff ADD CONSTRAINT FKStaff_2 FOREIGN KEY (AddressID) REFERENCES Address (ID);

ALTER TABLE Address ADD CONSTRAINT FKAddress_1 FOREIGN KEY (CityID) REFERENCES City (ID);

ALTER TABLE City ADD CONSTRAINT FKCity_1 FOREIGN KEY (CountryID) REFERENCES Country (ID);

ALTER TABLE Store ADD CONSTRAINT FKStore_1 FOREIGN KEY (AddressID) REFERENCES Address (ID);

ALTER TABLE Payment ADD CONSTRAINT FKPayment_3 FOREIGN KEY (StaffID) REFERENCES Staff (ID);