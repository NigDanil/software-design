CREATE TABLE IF NOT EXISTS Author (
  Name    varchar(255) NOT NULL, 
  Address varchar(255) NOT NULL, 
  URL     varchar(255), 
  PRIMARY KEY (Name, 
  Address));

CREATE TABLE IF NOT EXISTS Book (
  ISBN          varchar(255) NOT NULL, 
  PublisherName varchar(255) NOT NULL, 
  AuthorName    varchar(255) NOT NULL, 
  AuthorAddress varchar(255) NOT NULL, 
  Year          int4, 
  Title         varchar(255), 
  Price         numeric(19, 0), 
  PRIMARY KEY (ISBN));

CREATE TABLE IF NOT EXISTS Customer (
  Email   varchar(255) NOT NULL, 
  Name    varchar(255), 
  Phone   varchar(255), 
  Address varchar(255), 
  PRIMARY KEY (Email));

CREATE TABLE IF NOT EXISTS Publisher (
  Name    varchar(255) NOT NULL, 
  Address varchar(255), 
  Phone   varchar(255), 
  URL     int4, 
  PRIMARY KEY (Name));

CREATE TABLE IF NOT EXISTS ShoppingBasket (
  ID            SERIAL NOT NULL, 
  CustomerEmail varchar(255) NOT NULL, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS ShoppingBasket_Book (
  BookISBN         varchar(255) NOT NULL, 
  ShoppingBasketID int4 NOT NULL, 
  Count            int4);

CREATE TABLE IF NOT EXISTS Warehouse (
  Code    SERIAL NOT NULL, 
  Phone   varchar(255), 
  Address varchar(255), 
  PRIMARY KEY (Code));

CREATE TABLE IF NOT EXISTS Warehouse_Book (
  WarehouseCode int4 NOT NULL, 
  BookISBN      varchar(255) NOT NULL, 
  Count         int4);

ALTER TABLE Book ADD CONSTRAINT FKBook_1 FOREIGN KEY (PublisherName) REFERENCES Publisher (Name);

ALTER TABLE Book ADD CONSTRAINT FKBook_2 FOREIGN KEY (AuthorName, AuthorAddress) REFERENCES Author (Name, Address);

ALTER TABLE ShoppingBasket_Book ADD CONSTRAINT FKShoppingBa_1 FOREIGN KEY (BookISBN) REFERENCES Book (ISBN);

ALTER TABLE Warehouse_Book ADD CONSTRAINT FKWarehouse_1 FOREIGN KEY (BookISBN) REFERENCES Book (ISBN);

ALTER TABLE Warehouse_Book ADD CONSTRAINT FKWarehouse_2 FOREIGN KEY (WarehouseCode) REFERENCES Warehouse (Code);

ALTER TABLE ShoppingBasket_Book ADD CONSTRAINT FKShoppingBa_2 FOREIGN KEY (ShoppingBasketID) REFERENCES ShoppingBasket (ID);

ALTER TABLE ShoppingBasket ADD CONSTRAINT FKShoppingBa_3 FOREIGN KEY (CustomerEmail) REFERENCES Customer (Email);