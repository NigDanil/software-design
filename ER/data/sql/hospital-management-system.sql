CREATE TABLE IF NOT EXISTS Doctor (
  DSS            SERIAL NOT NULL, 
  Name           varchar(255), 
  Specialization varchar(255), 
  PRIMARY KEY (DSS));

CREATE TABLE IF NOT EXISTS Pacient (
  SS                 SERIAL NOT NULL, 
  Name               varchar(255), 
  Insurance          varchar(255), 
  Date_Admitted_date date, 
  Date_Checked_Out   date, 
  DoctorDSS          int4 NOT NULL, 
  PRIMARY KEY (SS));

CREATE TABLE IF NOT EXISTS Test (
  ID        SERIAL NOT NULL, 
  DoctorDSS int4 NOT NULL, 
  PacientSS int4 NOT NULL, 
  Name      varchar(255), 
  Test_Date date, 
  Test_Time date, 
  Result    varchar(255), 
  PRIMARY KEY (ID));

ALTER TABLE Pacient ADD CONSTRAINT FKPacient_1 FOREIGN KEY (DoctorDSS) REFERENCES Doctor (DSS);

ALTER TABLE Test ADD CONSTRAINT FKTest_1 FOREIGN KEY (PacientSS) REFERENCES Pacient (SS);

ALTER TABLE Test ADD CONSTRAINT FKTest_2 FOREIGN KEY (DoctorDSS) REFERENCES Doctor (DSS);