CREATE TABLE IF NOT EXISTS Department (
  Number            int4 NOT NULL, 
  Name              varchar(255) NOT NULL, 
  Locations         varchar(255), 
  NumberOfImployees int4, 
  PRIMARY KEY (Number, 
  Name));

CREATE TABLE IF NOT EXISTS Department_Managment (
  DepartmentNumber int4 NOT NULL, 
  DepartmentName   varchar(255) NOT NULL, 
  EmployeeSSN      int4 NOT NULL, 
  StartDate        date, 
  PRIMARY KEY (DepartmentNumber, 
  DepartmentName, 
  EmployeeSSN));

CREATE TABLE IF NOT EXISTS Dependent (
  EmployeeSSN  int4 NOT NULL, 
  Name         varchar(255) NOT NULL, 
  Sex          char(1), 
  BirthDate    date, 
  Relationship varchar(255), 
  PRIMARY KEY (Name));

CREATE TABLE IF NOT EXISTS Employee (
  SSN              SERIAL NOT NULL, 
  DepartmentNumber int4 NOT NULL, 
  DepartmentName   varchar(255) NOT NULL, 
  Supervisor       int4 NOT NULL, 
  BirthDate        date, 
  Sex              char(1), 
  Address          varchar(255), 
  Salary           int4, 
  FirstName        varchar(255), 
  Mlnit            varchar(255), 
  LastName         varchar(255), 
  PRIMARY KEY (SSN));

CREATE TABLE IF NOT EXISTS Project (
  Number           SERIAL NOT NULL, 
  DepartmentNumber int4 NOT NULL, 
  DepartmentName   varchar(255) NOT NULL, 
  Name             varchar(255), 
  Location         varchar(255), 
  PRIMARY KEY (Number));

CREATE TABLE IF NOT EXISTS Project_Employee (
  Hours         int4, 
  EmployeeSSN   int4 NOT NULL, 
  ProjectNumber int4 NOT NULL, 
  PRIMARY KEY (EmployeeSSN));

ALTER TABLE Employee ADD CONSTRAINT FKEmployee_1 FOREIGN KEY (DepartmentNumber, DepartmentName) REFERENCES Department (Number, Name);

ALTER TABLE Department_Managment ADD CONSTRAINT FKDepartment_1 FOREIGN KEY (DepartmentNumber, DepartmentName) REFERENCES Department (Number, Name);

ALTER TABLE Department_Managment ADD CONSTRAINT FKDepartment_2 FOREIGN KEY (EmployeeSSN) REFERENCES Employee (SSN);

ALTER TABLE Dependent ADD CONSTRAINT FKDependent_1 FOREIGN KEY (EmployeeSSN) REFERENCES Employee (SSN);

ALTER TABLE Project_Employee ADD CONSTRAINT FKProject_Em_1 FOREIGN KEY (EmployeeSSN) REFERENCES Employee (SSN);

ALTER TABLE Project ADD CONSTRAINT FKProject_1 FOREIGN KEY (DepartmentNumber, DepartmentName) REFERENCES Department (Number, Name);

ALTER TABLE Project_Employee ADD CONSTRAINT FKProject_Em_2 FOREIGN KEY (ProjectNumber) REFERENCES Project (Number);

ALTER TABLE Employee ADD CONSTRAINT Supervisor FOREIGN KEY (Supervisor) REFERENCES Employee (SSN);