CREATE TABLE IF NOT EXISTS Album (
  ID          SERIAL NOT NULL, 
  Title       varchar(120), 
  Description varchar(255), 
  "View"      int4, 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Comment (
  ID        SERIAL NOT NULL, 
  PhotoID   int4 NOT NULL, 
  Post_Date date, 
  Content   varchar(255), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Location (
  ID        SERIAL NOT NULL, 
  Name      varchar(200), 
  Shortname varchar(50), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Member (
  ID      SERIAL NOT NULL, 
  Name    varchar(255), 
  Phone   varchar(20), 
  Email   varchar(200), 
  Address varchar(255), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Photo (
  ID          SERIAL NOT NULL, 
  AlbumID     int4 NOT NULL, 
  LocationID  int4 NOT NULL, 
  MemberID    int4 NOT NULL, 
  Title       varchar(120), 
  Description varchar(255), 
  Privacy     varchar(20), 
  Upload_Date date, 
  "View"      int4, 
  Image_Path  varchar(50), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS "Tag" (
  ID    SERIAL NOT NULL, 
  Title varchar(120), 
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS Tag_Photo (
  TagID   int4 NOT NULL, 
  PhotoID int4 NOT NULL);

ALTER TABLE Photo ADD CONSTRAINT FKPhoto_1 FOREIGN KEY (LocationID) REFERENCES Location (ID);

ALTER TABLE Photo ADD CONSTRAINT FKPhoto_2 FOREIGN KEY (AlbumID) REFERENCES Album (ID);

ALTER TABLE Tag_Photo ADD CONSTRAINT FKTag_Photo_1 FOREIGN KEY (TagID) REFERENCES "Tag" (ID);

ALTER TABLE Tag_Photo ADD CONSTRAINT FKTag_Photo_2 FOREIGN KEY (PhotoID) REFERENCES Photo (ID);

ALTER TABLE Photo ADD CONSTRAINT FKPhoto_3 FOREIGN KEY (MemberID) REFERENCES Member (ID);

ALTER TABLE Comment ADD CONSTRAINT FKComment_1 FOREIGN KEY (PhotoID) REFERENCES Photo (ID);