<h4>Clone Project</h4>

- [ ] Clone with SSH

```bash
git@gitlab.com:NigDanil/software-design.git
```

- [ ] Clone with HTTPS

```bash
https://gitlab.com/NigDanil/software-design.git
```
---


<h3>Entity Relationship Model</h3>

- [ERM Models](ER/doc/content.md)

<h3>Unified Modeling Language</h3>

<p>Structural UML Diagrams:</p>

- [Class](UML/Class/doc/content.md)

- [Component](UML/Component/doc/content.md)

<p>Behavioral UML Diagrams:</p>

- [Activity](UML/Activity/doc/content.md)

- [Sequence](UML/Sequence/doc/content.md)

- [Use Case](UML/Use%20Case/doc/content.md)

- [State Machine](UML/State%20Machine/doc/content.md)

<h3>Data Flow Diagrams</h3>

- [DFD](DFD/doc/content.md)