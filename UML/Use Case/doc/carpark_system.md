[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Use Case](../data/img/Use_Case.png)

</details>

---

<h1>Carpark system</h1>

<details>
  <summary>Show Use Case Diagram</summary>

![Carpark system](../data/img/Carpark_System.jpg)

</details>
