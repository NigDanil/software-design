[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Use Case](../data/img/Use_Case.png)

</details>

---

<h1>Order process system</h1>

<details>
  <summary>Show Use Case Diagram</summary>

![Order process system](../data/img/Order_Process_System.jpg)

</details>
