[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Use Case](../data/img/Use_Case.png)

</details>

---

<h1>Software Development Management</h1>

<details>
  <summary>Show Use Case Diagram</summary>

![Software Development Management](../data/img/Software_Development_Management.jpg)

</details>
