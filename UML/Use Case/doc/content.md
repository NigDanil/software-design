[index](../../../README.md)

---

<h1>Use case diagram</h1>

<p>A use case diagram is a graphical depiction of a user's possible interactions with a system. A use case diagram shows various use cases and different types of users the system has and will often be accompanied by other types of diagrams as well. The use cases are represented by either circles or ellipses. The actors are often shown as stick figures.</p>

---

<h1>Use Case Diagrams</h1>


- [Carpark System](carpark_system.md)

- [Order Process System](order_process_system.md)

- [Passenger Service](passenger_service.md)

- [Business Airport](business_airport.md)

- [Software Development Management](software_development_management.md)
