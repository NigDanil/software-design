[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Use Case](../data/img/Use_Case.png)

</details>

---

<h1>Passenger service</h1>

<details>
  <summary>Show Use Case Diagram</summary>

![Passenger service](../data/img/Passenger_Service.jpg)

</details>
