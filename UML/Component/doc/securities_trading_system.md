[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Component](../data/img/Component.png)

</details>

---

<h1>Securities Trading System</h1>

<details>
  <summary>Show Component Diagram</summary>

![Securities Trading System](../data/img/Securities_Trading_System.jpg)

</details>
