[index](../../../README.md)

---

<h1>Component diagram</h1>

<p>A component diagram allows verification that a system's required functionality is acceptable. These diagrams are also used as a communication tool between the developer and stakeholders of the system. Programmers and developers use the diagrams to formalize a roadmap for the implementation, allowing for better decision-making about task assignment or needed skill improvements.</p>

<p> System administrators can use component diagrams to plan ahead, using the view of the logical software components and their relationships on the system.</p>

---

<h1>Component Diagrams</h1>


- [Web Store](web_store.md)

- [Ticket Selling System](ticket_selling_system.md)

- [Store Component](store_component.md)

- [Online Shop](online_shop.md)

- [Securities Trading System](securities_trading_system.md)
