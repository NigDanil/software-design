[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Component](../data/img/Component.png)

</details>

---

<h1>Ticket Selling System</h1>

<details>
  <summary>Show Component Diagram</summary>

![Ticket Selling System](../data/img/Ticket_Selling_System.jpg)

</details>
