<?php
require_once(realpath(dirname(__FILE__)) . '/Car.php');
require_once(realpath(dirname(__FILE__)) . '/Wheel.php');

/**
 * @access public
 * @author user
 * @package Car
 */
class Suspension {
	/**
	 * @AttributeType float
	 */
	private $springRate;
	/**
	 * @AttributeType Car
	 * /**
	 *  * @AssociationType Car
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Car_;
	/**
	 * @AttributeType Wheel
	 * /**
	 *  * @AssociationType Wheel
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Wheel_;
}
?>