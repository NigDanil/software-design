<?php
require_once(realpath(dirname(__FILE__)) . '/Car.php');
require_once(realpath(dirname(__FILE__)) . '/Wheel.php');

/**
 * @access public
 * @author user
 * @package Car
 */
class Brake {
	/**
	 * @AttributeType String
	 */
	private $type;
	/**
	 * @AttributeType Car
	 * /**
	 *  * @AssociationType Car
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Car_;
	/**
	 * @AttributeType Car\Wheel
	 * /**
	 *  * @AssociationType Car\Wheel
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Wheel_;

	/**
	 * @access public
	 */
	public function apply() {
		// Not yet implemented
	}
}
?>