<?php
require_once(realpath(dirname(__FILE__)) . '/GearBox.php');

/**
 * @access public
 * @author user
 * @package class_1\Car
 */
class GearBoxType {
	/**
	 * @AttributeType String
	 */
	private $name;
	/**
	 * @AttributeType String
	 */
	private $remarks;
	/**
	 * @AttributeType GearBox
	 * /**
	 *  * @AssociationType GearBox
	 *  * @AssociationMultiplicity 0..*
	 *  * /
	 */
	public $unnamed_GearBox_ = array();
}
?>