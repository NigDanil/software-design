<?php
require_once(realpath(dirname(__FILE__)) . '/Wheel.php');

/**
 * @access public
 * @author user
 * @package Car
 */
class Tire {
	/**
	 * @AttributeType float
	 */
	private $width;
	/**
	 * @AttributeType float
	 */
	private $airPressure;
	/**
	 * @AttributeType Wheel
	 * /**
	 *  * @AssociationType Wheel
	 *  * /
	 */
	public $unnamed_Wheel_;
}
?>