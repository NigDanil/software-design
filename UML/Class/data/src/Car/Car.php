<?php
require_once(realpath(dirname(__FILE__)) . '/Body.php');
require_once(realpath(dirname(__FILE__)) . '/Suspension.php');
require_once(realpath(dirname(__FILE__)) . '/Brake.php');
require_once(realpath(dirname(__FILE__)) . '/Engine.php');
require_once(realpath(dirname(__FILE__)) . '/GearBox.php');
require_once(realpath(dirname(__FILE__)) . '/CarModel.php');

/**
 * @access public
 * @author user
 * @package class_1\Car
 */
class Car {
	private $registrationNum;
	/**
	 * @AttributeType int
	 */
	private $year;
	/**
	 * @AttributeType String
	 */
	private $licenseNumber;
	/**
	 * @AttributeType Body
	 * /**
	 *  * @AssociationType Body
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Body_;
	/**
	 * @AttributeType Suspension
	 * /**
	 *  * @AssociationType Suspension
	 *  * @AssociationMultiplicity 1..*
	 *  * /
	 */
	public $unnamed_Suspension_ = array();
	/**
	 * @AttributeType Brake
	 * /**
	 *  * @AssociationType Brake
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Brake_;
	/**
	 * @AttributeType Engine
	 * /**
	 *  * @AssociationType Engine
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Engine_;
	/**
	 * @AttributeType Car\GearBox
	 * /**
	 *  * @AssociationType Car\GearBox
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_GearBox_;
	/**
	 * @AttributeType CarModel
	 * /**
	 *  * @AssociationType CarModel
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_CarModel_;

	/**
	 * @access public
	 */
	public function moveForward() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function moveBackward() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function stop() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function turnRight() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function turnLeft() {
		// Not yet implemented
	}
}
?>