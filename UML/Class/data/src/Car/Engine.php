<?php
require_once(realpath(dirname(__FILE__)) . '/Car.php');

/**
 * @access public
 * @author user
 * @package Car
 */
class Engine {
	/**
	 * @AttributeType float
	 */
	private $capacity;
	/**
	 * @AttributeType int
	 */
	private $numberOfCylinders;
	/**
	 * @AttributeType Car
	 * /**
	 *  * @AssociationType Car
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Car_;

	/**
	 * @access public
	 */
	public function start() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function brake() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function accerate() {
		// Not yet implemented
	}
}
?>