<?php
require_once(realpath(dirname(__FILE__)) . '/Car.php');
require_once(realpath(dirname(__FILE__)) . '/GearBoxType.php');

/**
 * @access public
 * @author user
 * @package class_1\Car
 */
class GearBox {
	/**
	 * @AttributeType float[]
	 */
	private $gearRatio;
	/**
	 * @AttributeType int
	 */
	private $currentGear;
	/**
	 * @AttributeType Car
	 * /**
	 *  * @AssociationType Car
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Car_;
	/**
	 * @AttributeType GearBoxType
	 * /**
	 *  * @AssociationType GearBoxType
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_GearBoxType_;

	/**
	 * @access public
	 */
	public function shiftUp() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function shiftDown() {
		// Not yet implemented
	}
}
?>