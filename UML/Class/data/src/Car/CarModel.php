<?php
require_once(realpath(dirname(__FILE__)) . '/Car.php');

/**
 * @access public
 * @author user
 * @package Car
 */
class CarModel {
	/**
	 * @AttributeType String
	 */
	private $title;
	/**
	 * @AttributeType Car
	 * /**
	 *  * @AssociationType Car
	 *  * @AssociationMultiplicity 0..*
	 *  * /
	 */
	public $unnamed_Car_ = array();
}
?>