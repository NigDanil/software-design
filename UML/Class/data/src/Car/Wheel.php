<?php
require_once(realpath(dirname(__FILE__)) . '/Brake.php');
require_once(realpath(dirname(__FILE__)) . '/Suspension.php');
require_once(realpath(dirname(__FILE__)) . '/Tire.php');

/**
 * @access public
 * @author user
 * @package Car
 */
class Wheel {
	/**
	 * @AttributeType float
	 */
	private $diameter;
	/**
	 * @AttributeType Brake
	 * /**
	 *  * @AssociationType Brake
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Brake_;
	/**
	 * @AttributeType Suspension
	 * /**
	 *  * @AssociationType Suspension
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Suspension_;
	/**
	 * @AttributeType Tire
	 * /**
	 *  * @AssociationType Tire
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Tire_;
}
?>