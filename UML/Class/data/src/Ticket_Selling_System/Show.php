<?php
require_once(realpath(dirname(__FILE__)) . '/Performance.php');

/**
 * @access public
 * @author user
 * @package class_1\Ticket_Selling_System
 */
class Show {
	/**
	 * @AttributeType String
	 */
	private $title;
	/**
	 * @AttributeType Performance
	 * /**
	 *  * @AssociationType Performance
	 *  * @AssociationMultiplicity 1..*
	 *  * /
	 */
	public $performance = array();
}
?>