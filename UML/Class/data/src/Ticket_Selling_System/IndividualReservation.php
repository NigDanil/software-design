<?php
require_once(realpath(dirname(__FILE__)) . '/Ticket.php');
require_once(realpath(dirname(__FILE__)) . '/Reservation.php');

/**
 * @access public
 * @author user
 * @package class_1\Ticket_Selling_System
 */
class IndividualReservation extends Reservation {
	/**
	 * @AttributeType class\Ticket_Selling_System\Ticket
	 * /**
	 *  * @AssociationType class\Ticket_Selling_System\Ticket
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Ticket_;
}
?>