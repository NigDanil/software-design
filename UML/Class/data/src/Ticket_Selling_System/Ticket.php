<?php
require_once(realpath(dirname(__FILE__)) . '/IndividualReservation.php');
require_once(realpath(dirname(__FILE__)) . '/SubscriptionSeries.php');
require_once(realpath(dirname(__FILE__)) . '/Performance.php');
require_once(realpath(dirname(__FILE__)) . '/Customer.php');

/**
 * @access public
 * @author user
 * @package class_1\Ticket_Selling_System
 */
class Ticket {
	/**
	 * @AttributeType boolean
	 */
	private $available;
	/**
	 * @AttributeType IndividualReservation
	 * /**
	 *  * @AssociationType IndividualReservation
	 *  * @AssociationMultiplicity 0..1
	 *  * /
	 */
	public $unnamed_IndividualReservation_;
	/**
	 * @AttributeType SubscriptionSeries
	 * /**
	 *  * @AssociationType SubscriptionSeries
	 *  * @AssociationMultiplicity 0..1
	 *  * /
	 */
	public $unnamed_SubscriptionSeries_;
	/**
	 * @AttributeType class\Ticket_Selling_System\Performance
	 * /**
	 *  * @AssociationType class\Ticket_Selling_System\Performance
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $unnamed_Performance_;

	/**
	 * @access public
	 * @param Customer c
	 * @ParamType c Customer
	 */
	public function sell(Customer &$c) {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function exchange() {
		// Not yet implemented
	}
}
?>