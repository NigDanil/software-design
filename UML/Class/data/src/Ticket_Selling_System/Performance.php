<?php
require_once(realpath(dirname(__FILE__)) . '/Show.php');
require_once(realpath(dirname(__FILE__)) . '/Ticket.php');

/**
 * @access public
 * @author user
 * @package class_1\Ticket_Selling_System
 */
class Performance {
	/**
	 * @AttributeType date
	 */
	private $performanceDate;
	/**
	 * @AttributeType int
	 */
	private $performanceTime;
	/**
	 * @AttributeType Show
	 * /**
	 *  * @AssociationType Show
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $show;
	/**
	 * @AttributeType Ticket
	 * /**
	 *  * @AssociationType Ticket
	 *  * @AssociationMultiplicity *
	 *  * /
	 */
	public $unnamed_Ticket_ = array();
}
?>