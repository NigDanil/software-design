<?php
require_once(realpath(dirname(__FILE__)) . '/Customer.php');

/**
 * @access public
 * @author user
 * @package class_1\Ticket_Selling_System
 */
abstract class Reservation {
	/**
	 * @AttributeType date
	 */
	private $reservationDate;
	/**
	 * @AttributeType Customer
	 * /**
	 *  * @AssociationType Customer
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $owner;
}
?>