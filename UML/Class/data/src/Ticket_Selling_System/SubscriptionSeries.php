<?php
require_once(realpath(dirname(__FILE__)) . '/Ticket.php');
require_once(realpath(dirname(__FILE__)) . '/Reservation.php');

/**
 * @access public
 * @author user
 * @package class_1\Ticket_Selling_System
 */
class SubscriptionSeries extends Reservation {
	/**
	 * @AttributeType int
	 */
	private $series;
	/**
	 * @AttributeType Ticket
	 * /**
	 *  * @AssociationType Ticket
	 *  * @AssociationMultiplicity 2..5
	 *  * /
	 */
	public $unnamed_Ticket_ = array();
}
?>