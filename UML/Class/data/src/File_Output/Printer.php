<?php
require_once(realpath(dirname(__FILE__)) . '/Output_Device.php');

/**
 * @access public
 * @author user
 * @package class_1\File_Output
 */
class Printer extends Output_Device {
	private $ready;
	private $buffer;

	/**
	 * @access public
	 */
	public function formFeed() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function lineFeed() {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @param String s
	 * @ParamType s String
	 */
	public function printLine($s) {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @param String s
	 * @ParamType s String
	 */
	public function display($s) {
		// Not yet implemented
	}
}
?>