<?php
require_once(realpath(dirname(__FILE__)) . '/Output_Device.php');

/**
 * @access public
 * @author user
 * @package File_Output
 */
class File {
	/**
	 * @AttributeType String
	 */
	private $name;
	/**
	 * @AttributeType User
	 */
	private $owner;

	/**
	 * @access public
	 * @param Name n
	 * @param User o
	 * @ParamType n Name
	 * @ParamType o User
	 */
	public function create(Name &$n, User &$o) {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function delete() {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @param Output_Device d
	 * @ParamType d Output Device
	 */
	public function display(Output_Device &$d) {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function execute() {
		// Not yet implemented
	}
}
?>