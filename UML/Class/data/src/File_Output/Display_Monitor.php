<?php
require_once(realpath(dirname(__FILE__)) . '/Output_Device.php');

/**
 * @access public
 * @author user
 * @package File_Output
 */
class Display_Monitor extends Output_Device {
	private $color;
	private $brightness;
	private $contrast;
	private $graphicsMemory;

	/**
	 * @access public
	 */
	public function setBrightness() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function setContrast() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function setColor() {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @param String s
	 * @ParamType s String
	 */
	public function display($s) {
		// Not yet implemented
	}
}
?>