<?php
require_once(realpath(dirname(__FILE__)) . '/Answering_Machine.php');
require_once(realpath(dirname(__FILE__)) . '/Caller_Id.php');
require_once(realpath(dirname(__FILE__)) . '/Ringer.php');

/**
 * @access public
 * @author user
 * @package class_1\Telephone
 */
class Telephone {
	/**
	 * @AttributeType boolean
	 */
	private $hook = true;
	/**
	 * @AttributeType int
	 */
	private $connection = 0;
	/**
	 * @AttributeType Telephone\Answering Machine
	 * /**
	 *  * @AssociationType Telephone\Answering Machine
	 *  * @AssociationMultiplicity 1
	 *  * @AssociationKind Aggregation
	 *  * /
	 */
	public $unnamed_Answering_Machine_;
	/**
	 * @AttributeType Caller Id
	 * /**
	 *  * @AssociationType Caller Id
	 *  * @AssociationMultiplicity 1
	 *  * @AssociationKind Aggregation
	 *  * /
	 */
	public $unnamed_Caller_Id_;
	/**
	 * @AttributeType Ringer
	 * /**
	 *  * @AssociationType Ringer
	 *  * @AssociationKind Aggregation
	 *  * /
	 */
	public $unnamed_Ringer_;

	/**
	 * @access public
	 */
	public function onHook() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function offHook() {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @param int n
	 * @ParamType n int
	 */
	public function dial($n) {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @param boolean status
	 * @ParamType status boolean
	 */
	public function setCallerId($status) {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @param boolean status
	 * @ParamType status boolean
	 */
	public function setAnsMachine($status) {
		// Not yet implemented
	}
}
?>