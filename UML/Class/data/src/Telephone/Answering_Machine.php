<?php
require_once(realpath(dirname(__FILE__)) . '/Telephone.php');
require_once(realpath(dirname(__FILE__)) . '/Message.php');

/**
 * @access public
 * @author user
 * @package class_1\Telephone
 */
class Answering_Machine {
	/**
	 * @AttributeType boolean
	 */
	private $status;
	/**
	 * @AttributeType Telephone
	 * /**
	 *  * @AssociationType Telephone
	 *  * /
	 */
	public $unnamed_Telephone_;
	/**
	 * @AttributeType Telephone\Message
	 * /**
	 *  * @AssociationType Telephone\Message
	 *  * @AssociationKind Composition
	 *  * /
	 */
	public $recordedMsgs;

	/**
	 * @access public
	 */
	public function set() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function reset() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function playback() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function record() {
		// Not yet implemented
	}
}
?>