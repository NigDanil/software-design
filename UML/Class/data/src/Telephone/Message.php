<?php
require_once(realpath(dirname(__FILE__)) . '/Answering_Machine.php');

/**
 * @access public
 * @author user
 * @package class_1\Telephone
 */
class Message {
	/**
	 * @AttributeType AudioStream
	 */
	private $content;
	/**
	 * @AttributeType Answering Machine
	 * /**
	 *  * @AssociationType Answering Machine
	 *  * /
	 */
	public $unnamed_Answering_Machine_;
}
?>