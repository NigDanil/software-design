<?php
require_once(realpath(dirname(__FILE__)) . '/Telephone.php');

/**
 * @access public
 * @author user
 * @package class_1\Telephone
 */
class Line {
	/**
	 * @AttributeType boolean
	 */
	private $busy;
	/**
	 * @AttributeType Telephone
	 * /**
	 *  * @AssociationType Telephone
	 *  * @AssociationMultiplicity 0..*
	 *  * /
	 */
	public $connectedPhones = array();

	/**
	 * @access public
	 * @param int n
	 * @ParamType n int
	 */
	public function dial($n) {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function offHook() {
		// Not yet implemented
	}

	/**
	 * @access public
	 */
	public function onHook() {
		// Not yet implemented
	}
}
?>