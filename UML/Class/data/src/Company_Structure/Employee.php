<?php
require_once(realpath(dirname(__FILE__)) . '/Department.php');

/**
 * @access public
 * @author user
 * @package class_1\Company_Structure
 */
class Employee {
	/**
	 * @AttributeType String
	 */
	private $name;
	/**
	 * @AttributeType String
	 */
	private $title;
	/**
	 * @AttributeType Department
	 * /**
	 *  * @AssociationType Department
	 *  * /
	 */
	public $unnamed_Department_;
	/**
	 * @AttributeType Department
	 * /**
	 *  * @AssociationType Department
	 *  * /
	 */
	public $manage;

	/**
	 * @access public
	 * @return String
	 * @ReturnType String
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @access public
	 * @param String n
	 * @ParamType n String
	 */
	public function setName($n) {
		// Not yet implemented
	}

	/**
	 * @access public
	 * @return String
	 * @ReturnType String
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @access public
	 * @param String t
	 * @ParamType t String
	 */
	public function setTitle($t) {
		// Not yet implemented
	}
}
?>