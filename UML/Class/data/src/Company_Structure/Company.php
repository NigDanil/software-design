<?php
require_once(realpath(dirname(__FILE__)) . '/Office.php');
require_once(realpath(dirname(__FILE__)) . '/Department.php');

/**
 * @access public
 * @author user
 * @package class_1\Company_Structure
 */
class Company {
	/**
	 * @AttributeType String
	 */
	private $name;
	/**
	 * @AttributeType Office
	 * /**
	 *  * @AssociationType Office
	 *  * @AssociationMultiplicity *
	 *  * @AssociationKind Composition
	 *  * /
	 */
	public $unnamed_Office_ = array();
	/**
	 * @AttributeType Department
	 * /**
	 *  * @AssociationType Department
	 *  * @AssociationMultiplicity *
	 *  * @AssociationKind Composition
	 *  * /
	 */
	public $unnamed_Department_ = array();

	/**
	 * @access public
	 * @return String
	 * @ReturnType String
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @access public
	 * @param String n
	 * @ParamType n String
	 */
	public function setName($n) {
		// Not yet implemented
	}
}
?>