<?php
require_once(realpath(dirname(__FILE__)) . '/Company.php');
require_once(realpath(dirname(__FILE__)) . '/Department.php');

/**
 * @access public
 * @author user
 * @package class_1\Company_Structure
 */
class Office {
	/**
	 * @AttributeType String
	 */
	private $address;
	/**
	 * @AttributeType Company
	 * /**
	 *  * @AssociationType Company
	 *  * /
	 */
	public $unnamed_Company_;
	/**
	 * @AttributeType Department
	 * /**
	 *  * @AssociationType Department
	 *  * @AssociationMultiplicity *
	 *  * /
	 */
	public $unnamed_Department_ = array();

	/**
	 * @access public
	 * @return String
	 * @ReturnType String
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @access public
	 * @param String a
	 * @ParamType a String
	 */
	public function setAddress($a) {
		// Not yet implemented
	}
}
?>