<?php
require_once(realpath(dirname(__FILE__)) . '/Employee.php');
require_once(realpath(dirname(__FILE__)) . '/Company.php');
require_once(realpath(dirname(__FILE__)) . '/Office.php');

/**
 * @access public
 * @author user
 * @package class_1\Company_Structure
 */
class Department {
	/**
	 * @AttributeType String
	 */
	private $name;
	/**
	 * @AttributeType Employee
	 * /**
	 *  * @AssociationType Employee
	 *  * @AssociationMultiplicity 1
	 *  * /
	 */
	public $managedBy;
	/**
	 * @AttributeType Company
	 * /**
	 *  * @AssociationType Company
	 *  * /
	 */
	public $unnamed_Company_;
	/**
	 * @AttributeType Employee
	 * /**
	 *  * @AssociationType Employee
	 *  * @AssociationMultiplicity 1..*
	 *  * /
	 */
	public $unnamed_Employee_ = array();
	/**
	 * @AttributeType Office
	 * /**
	 *  * @AssociationType Office
	 *  * @AssociationMultiplicity *
	 *  * /
	 */
	public $unnamed_Office_ = array();

	/**
	 * @access public
	 * @return String
	 * @ReturnType String
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @access public
	 * @param String n
	 * @ParamType n String
	 */
	public function setName($n) {
		// Not yet implemented
	}
}
?>