[index](../../../README.md)

--- 

<h1>Class diagram</h1>

<p>In software engineering, a class diagram in the Unified Modeling Language (UML) is a type of static structure diagram that describes the structure of a system by showing the system's classes, their attributes, operations (or methods), and the relationships among objects.
</p>

<p>The class diagram is the main building block of object-oriented modeling. It is used for general conceptual modeling of the structure of the application, and for detailed modeling, translating the models into programming code. Class diagrams can also be used for data modeling.[1] The classes in a class diagram represent both the main elements, interactions in the application, and the classes to be programmed.
</p>

<p>In the diagram, classes are represented with boxes that contain three compartments:

- The top compartment contains the name of the class. It is printed in bold and centered, and the first letter is capitalized.

- The middle compartment contains the attributes of the class. They are left-aligned and the first letter is lowercase.

- The bottom compartment contains the operations the class can execute. They are also left-aligned and the first letter is lowercase.</p>

<p>A class with three compartments.
In the design of a system, a number of classes are identified and grouped together in a class diagram that helps to determine the static relations between them. In detailed modeling, the classes of the conceptual design are often split into subclasses.
</p>

<p>In order to further describe the behavior of systems, these class diagrams can be complemented by a state diagram or UML state machine.
</p>

---

<h1>Class Diagrams</h1>


- [Telephone](telephone.md)

- [File Output](file_output.md)

- [Ticket Selling System](ticket_selling_system.md)

- [Company Structure](company_structure.md)

- [Car](car.md)