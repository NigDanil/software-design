[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Class Diagram</summary>

![Class](../data/img/Class.png)

</details>

---

<h1>Company Structure</h1>

<details>
  <summary>Show Class Diagram</summary>

![Company Structure](../data/img/Company_Structure.jpg)

</details>
