[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Class Diagram</summary>

![Class](../data/img/Class.png)

</details>

---

<h1>File Output</h1>

<details>
  <summary>Show Class Diagram</summary>

![Telephone](../data/img/File_Output.jpg)

</details>
