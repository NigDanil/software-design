[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![State Machine](../data/img/State_Machine.png)

</details>

---

<h1>Auction</h1>

<details>
  <summary>Show State Machine Diagram</summary>

![Auction](../data/img/Auction.jpg)

</details>
