[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![State Machine](../data/img/State_Machine.png)

</details>

---

<h1>Choice Node</h1>

<details>
  <summary>Show State Machine Diagram</summary>

![Choice Node](../data/img/Choice_Node.jpg)

</details>
