[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![State Machine](../data/img/State_Machine.png)

</details>

---

<h1>Phone Call</h1>

<details>
  <summary>Show State Machine Diagram</summary>

![Phone Call](../data/img/Phone_Call.jpg)

</details>
