[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Sequence](../data/img/Sequence.png)

</details>

---

<h1>Hospital Bed Allocation</h1>

<details>
  <summary>Show Sequence Diagram</summary>

![Hospital Bed Allocation](../data/img/Hospital_Bed_Allocation.jpg)

</details>
