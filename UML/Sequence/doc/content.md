[index](../../../README.md)

---
<h1>Sequence diagram</h1>

<p>A sequence diagram or system sequence diagram (SSD) shows process interactions arranged in time sequence in the field of software engineering. It depicts the processes involved and the sequence of messages exchanged between the processes needed to carry out the functionality. Sequence diagrams are typically associated with use case realizations in the 4+1 architectural view model of the system under development. Sequence diagrams are sometimes called event diagrams or event scenarios.
</p>

<p>For a particular scenario of a use case, the diagrams show the events that external actors generate, their order, and possible inter-system events. All systems are treated as a black box; the diagram places emphasis on events that cross the system boundary from actors to systems. A system sequence diagram should be done for the main success scenario of the use case, and frequent or complex alternative scenarios.</p>

---

<h1>Sequence Diagrams</h1>


- [Loop Fragment](loop_fragment.md)

- [Appointment](appointment.md)

- [Hospital Bed Allocation](hospital_bed_allocation.md)

- [Make Phone Call](make_phone_call.md)

- [Place Order](place_order.md)
