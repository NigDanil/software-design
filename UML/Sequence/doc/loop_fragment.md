[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Sequence](../data/img/Sequence.png)

</details>

---

<h1>Loop Fragment</h1>

<details>
  <summary>Show Sequence Diagram</summary>

![Loop Fragment](../data/img/Loop_Fragment.jpg)

</details>
