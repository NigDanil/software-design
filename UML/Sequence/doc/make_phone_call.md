[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Sequence](../data/img/Sequence.png)

</details>

---

<h1>Make Phone Call</h1>

<details>
  <summary>Show Sequence Diagram</summary>

![Make Phone Call](../data/img/Make_Phone_Call.jpg)

</details>
