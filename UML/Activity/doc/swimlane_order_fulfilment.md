[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Activity](../data/img/Activity.png)

</details>

---

<h1>Swimlane Order Fulfilment</h1>

<details>
  <summary>Show Activity Diagram</summary>

![Swimlane Order Fulfilment](../data/img/Swimlane_Order_Fulfilment.jpg)

</details>
