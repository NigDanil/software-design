[index](../../../README.md)

---

<h1>Activity diagram</h1>

<p>Activity diagrams are graphical representations of workflows of stepwise activities and actions with support for choice, iteration and concurrency. In the Unified Modeling Language, activity diagrams are intended to model both computational and organizational processes (i.e., workflows), as well as the data flows intersecting with the related activities.</p>

<p>Although activity diagrams primarily show the overall flow of control, they can also include elements showing the flow of data between activities through one or more data stores.</p>



---
<h1>List of Activity Diagram</h1>


- [ATM](atm.md)

- [Article Submission](article_submission.md)

- [Order Process System](order_processing.md)

- [Swimlane Order Fulfilment](swimlane_order_fulfilment.md)

- [Temperature Converter](temperature_converter.md)
