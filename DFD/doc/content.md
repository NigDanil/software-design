[index](../../README.md)

---

<h1>Dataflow programming</h1>

<p>In computer programming, dataflow programming is a programming paradigm that models a program as a directed graph of the data flowing between operations, thus implementing dataflow principles and architecture. Dataflow programming languages share some features of functional languages, and were generally developed in order to bring some functional concepts to a language more suitable for numeric processing. Some authors use the term datastream instead of dataflow to avoid confusion with dataflow computing or dataflow architecture, based on an indeterministic machine paradigm. Dataflow programming was pioneered by Jack Dennis and his graduate students at MIT in the 1960s.</p>

---

<h1>Dataflow Diagrams</h1>


- [Food Ordering System](food_ordering_system.md)

- [Customer Service System](customer_service_system.md)

- [Supermarket App](supermarket_app.md)

- [Vehicle Maintenance Depot](vehicle_maintenance_depot.md)

- [Customer Service System Railway Company](customer_service_system_railway_company.md)
