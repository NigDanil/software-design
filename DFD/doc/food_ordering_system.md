[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Data Flow](../data/img/Data_Flow.png)

</details>

---

<h1>Food Ordering System (Context DFD)</h1>

<details>
  <summary>Show (Context DFD)</summary>

![Food Ordering System (Context DFD)](../data/img/Food_Ordering_System_Context_DFD.jpg)

</details>

<h1>Food Ordering System</h1>

<details>
  <summary>Show DFD</summary>

![Food Ordering System (Context DFD)](../data/img/Food_Ordering_System.jpg)

</details>
