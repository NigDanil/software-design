[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Data Flow](../data/img/Data_Flow.png)

</details>

---

<h1>Supermarket App (Context DFD)</h1>

<details>
  <summary>Show (Context DFD)</summary>

![Customer Service System Context DFD](../data/img/Supermarket_App_Context_DFD.jpg)

</details>

<h1>Supermarket App</h1>

<details>
  <summary>Show DFD</summary>

![Customer Service System](../data/img/Supermarket_App.jpg)

</details>