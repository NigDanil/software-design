[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Data Flow](../data/img/Data_Flow.png)

</details>

---

<h1>Customer Service System (Context DFD)</h1>

<details>
  <summary>Show (Context DFD)</summary>

![Customer Service System Context DFD](../data/img/Customer_Service_System_Context_DFD.jpg)

</details>

<h1>Customer Service System</h1>

<details>
  <summary>Show DFD</summary>

![Customer Service System](../data/img/Customer_Service_System.jpg)

</details>