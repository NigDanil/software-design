[Content](content.md)

---

<h1>Cheat sheet</h1>

<details>
  <summary>Show Cheat sheet</summary>

![Data Flow](../data/img/Data_Flow.png)

</details>

---

<h1>Customer Service System (Railway Company)</h1>

<details>
  <summary>Show DFD</summary>

![Customer Service System (Railway Company)](../data/img/Customer_Service_System_Railway_Company.jpg)

</details>

